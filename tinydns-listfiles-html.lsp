<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"delete", "edit", "editfile"}, session) %>
<% htmlviewfunctions.displaycommandresults({"newfile", "startstop"}, session, true) %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Configuration"}), page_info)
local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Edit/View Existing Domains"}), page_info, htmlviewfunctions.incrementheader(header_level))
%>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Size</th>
		<th>Last Modified</th>
		<th>File</th>
	</tr>
</thead><tbody>
<% local filename = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,file in ipairs(form.value) do %>
	<tr>
		<td>
			<%
			filename.value = file.filename
			htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename}, label="", option="View", action="view"}), page_info, -1)
			if viewlibrary.check_permission("edit") then
				htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Edit", action="edit"}), page_info, -1)
			end
			if viewlibrary.check_permission("editfile") then
				htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Expert", action="editfile"}), page_info, -1)
			end
			if viewlibrary.check_permission("delete") then
				htmlviewfunctions.displayitem(cfe({type="form", value={filename=filename}, label="", option="Delete", action="delete"}), page_info, -1)
			end
			%>
		</td>
		<td><span class="hide"><%= html.html_escape(file.size or 0) %>b</span><%= format.formatfilesize(file.size) %></td>
		<td><%= format.formattime(file.mtime) %></td>
		<td><%= html.html_escape(string.gsub(file.filename, "^.*/", "")) %></td>
	</tr>
<% end %>
</tbody></table>
<% if #form.value == 0 then %>
	No domains defined
<% end %>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("newfile") then
	local newfileform = viewlibrary.dispatch_component("newfile", nil, true)
	newfileform.action = page_info.script .. page_info.prefix .. page_info.controller .. "/newfile"
	newfileform.label = "Create New Domain"
	htmlviewfunctions.displayitem(newfileform, page_info, header_level2)
end %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("startstop") then
	viewlibrary.dispatch_component("startstop")
end %>
