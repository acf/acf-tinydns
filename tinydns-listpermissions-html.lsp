<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#userlist").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		$("#rolelist").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"edituserpermissions", "editrolepermissions"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="User Permissions"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<table id="userlist" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>User</th>
		<th>Permissions</th>
	</tr>
</thead><tbody>
<% local userid = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,user in ipairs(view.value.user) do %>
	<% userid.value = user.id %>
	<tr>
		<td><% htmlviewfunctions.displayitem(cfe({type="link", value={userid=userid, redir=redir}, label="", option="Edit", action="edituserpermissions"}), page_info, -1) %></td>
		<td><%= html.html_escape(user.id) %></td>
		<td>
		<% for y,allowed in pairs(user.allowed) do
			print(html.html_escape(allowed), "<br/>")
		end %>
		</td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level2) %>

<% htmlviewfunctions.displaysectionstart(cfe({label="Role Permissions"}), page_info, header_level2) %>
<table id="rolelist" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Role</th>
		<th>Permissions</th>
	</tr>
</thead><tbody>
<% local rolecfe = cfe({ type="hidden", value="" }) %>
<% for i,role in ipairs(view.value.role) do %>
	<% rolecfe.value = role.id %>
	<tr>
		<td><% htmlviewfunctions.displayitem(cfe({type="link", value={role=rolecfe, redir=redir}, label="", option="Edit", action="editrolepermissions"}), page_info, -1) %></td>
		<td><%= html.html_escape(role.id) %></td>
		<td>
		<% for y,allowed in pairs(role.allowed) do
			print(html.html_escape(allowed), "<br/>")
		end %>
		</td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
