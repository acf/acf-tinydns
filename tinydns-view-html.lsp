<% local view, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
        if (typeof jQuery == 'undefined') {
                document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	var last_phrase = "";
	function filterPage(){
		var phrase = $("#filter").val();
		if (phrase != last_phrase){
			last_phrase = phrase;
			$("#records li").each(function(){
				var elem = jQuery(this);
				if (elem.text().indexOf(phrase)>=0) {
					elem.show();
				} else {
					elem.hide();
				}
			});
		}
	}
	function toggleEntry(){
		$(this).find("table").toggle();
		$(this).find("pre").toggle();
	}
	function editEntry(){
		window.location.href = "edit?" + this.id;
	}
	var filterTimer;
	$(function(){
		$("#filter").keyup(function(){
			window.clearTimeout(filterTimer);
			filterTimer = window.setTimeout("filterPage();",250);
		});
		$("#filter-list").submit(function(){
			return false;
		}).focus();
		$("#records pre").hide();
		$("#records li").not(":has(ul)").click(toggleEntry);
		$("#records li").not(":has(ul)").dblclick(editEntry);
		$("#locations li").not(":has(ul)").dblclick(editEntry);
	});
</script>

<% if view.filename then view.label = view.label.." for "..string.gsub(view.filename, "^.*/", "") end %>
<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Locations"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<ul id="locations">
<% local currentloc = ""
for i,loc in ipairs(view.value) do
	if loc.type == '%' then
		if currentloc ~= loc[1] then
			if currentloc ~= "" then %>
				</ul>
			<% end %>
			<li><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/places/start-here.png' width='16' height='16' alt> <b><%= html.html_escape(loc[1]) %></b></li>
			<ul style='margin-left:30px';>
		<% end
		currentloc = loc[1] %>
		<li id='filename=<%= html.html_escape(loc.filename) %>;linenumber=<%= html.html_escape(loc.linenumber) %>'>
		<%= html.html_escape(loc[2]) %><% if loc[2] == "" then io.write('*') end %>
		<% if (loc.errtxt) then %><p class='error'><%= string.gsub(html.html_escape(loc.errtxt), "\n", "<br/>") %></p><% end %>
		</li>
	<% end
end %>
	</ul>
<% if currentloc == "" then %>
	<p>No locations defined</p>
<% else %>
	</ul>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level2) %>

<%
local function doListIndents(next, indent)
	local newentry = {}
	for mt in string.gmatch(next, "([^.]+)") do
		table.insert(newentry, mt)
	end
	local revnewentry = {}
	for j=#newentry,1,-1 do
		table.insert(revnewentry, newentry[j])
	end
	local i=1
	while indent[i] and revnewentry[i] == indent[i] do
		i=i+1
	end
	local ending = #indent-(i-1)
	local starting = #newentry-(i-1)
	for j=1,ending do
		io.write("</li></ul>\n")
	end
	for j=1,starting do
		io.write("<ul><li style='margin-left:10px;'><strong>")
		io.write(html.html_escape(table.concat(newentry, ".", #newentry-(i+j-2), #newentry)))
		io.write("</strong>\n")
	end
	return revnewentry
end
%>

<% htmlviewfunctions.displaysectionstart(cfe({label="Records"}), page_info, header_level2) %>
<form id="filter-list">Filter: <input name="filter" id="filter" value="" maxlength="30" size="30" type="text"></form>
<ul id="records">
<% local indent = {}
for j,entry in ipairs(view.value) do
	if entry.type ~= '%' then
		indent = doListIndents(entry[1], indent) %>
		<ul><li style='margin-left:10px;' id='filename=<%= html.html_escape(entry.filename) %>;linenumber=<%= html.html_escape(entry.linenumber) %>'>
		<img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/devices/computer.png' width='16' height='16'><%= html.html_escape(entry.label) %><br/>
		<pre><%= html.html_escape(entry.configline) %></pre>
		<table style='margin-left:<%= html.html_escape(tostring(7-#indent)) %>0px;'>
		<% for k=2,#entry do
			local option = entry[k]
			if (option) and option ~= "" then %>
				<tr><td width='160px' style='border:none;'><%= html.html_escape(view.fieldlabels[entry.type][k]) %>:</td>
				<td style='border:none;'><%= html.html_escape(option) %></td></tr>
			<% end
		end %>
		</table>
		<% htmlviewfunctions.displayinfo(entry) %>
		</li></ul>
	<% end
end
if #indent == 0 then %>
	</ul>
	<p>No records defined</p>
<% else
	doListIndents("", indent) %>
	</ul>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
